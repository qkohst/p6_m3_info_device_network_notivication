**After cloning the project, run the following command:**

```
$ cordova platform add android
$ cordova plugin add cordova-plugin-device
```

**Then you can use the following command to run the program**

```
$ phonegap serve
```


**Preview**

![WhatsApp_Image_2020-11-07_at_11.07.04_PM](/uploads/8a71c5ad6b99759de7259ee30000d27b/WhatsApp_Image_2020-11-07_at_11.07.04_PM.jpeg)!
![WhatsApp_Image_2020-11-07_at_11.07.04_PM__1_](/uploads/1d53ea04eabbd4c171d7270ad89314df/WhatsApp_Image_2020-11-07_at_11.07.04_PM__1_.jpeg)!
